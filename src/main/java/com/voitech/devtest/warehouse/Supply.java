/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.voitech.devtest.warehouse;

/**
 *
 * @author wojtek
 */
public class Supply implements Supplyable{

    private Warehouse warehouse;
    private ProductType productType;

    public Supply(Warehouse warehouse, ProductType productType) {
        this.warehouse = warehouse;
        this.productType = productType;
    }
 
    
    
    @Override
    public void sale() {
        warehouse.sale(productType);
    }

    @Override
    public ProductType getProductType() {
        return this.productType;
    }
    
}
