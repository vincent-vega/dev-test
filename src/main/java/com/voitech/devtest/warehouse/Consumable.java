package com.voitech.devtest.warehouse;
public interface Consumable {

	void buy();
	
	ProductType getProductType();
}
