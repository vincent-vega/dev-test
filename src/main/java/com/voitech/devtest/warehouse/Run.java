/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.voitech.devtest.warehouse;

/**
 *
 * @author wojtek
 */
public class Run {
    
    public static final void main(String args[]){
        
        Warehouse warehouse = ConcreteWarehouse.getInstance();
        
        Consumable consumer1 = new ConcreteConsumer(new Consume(warehouse,ProductType.APPLE)).getConsumable();
        Consumable consumer2 = new ConcreteConsumer(new Consume(warehouse,ProductType.ORANGE)).getConsumable();
        Consumable consumer3 = new ConcreteConsumer(new Consume(warehouse,ProductType.BANANA)).getConsumable();
        
        warehouse.registerConsumer(consumer1);
        warehouse.registerConsumer(consumer2);
        warehouse.registerConsumer(consumer3);
        
        
        Supplyable supplier1 = new ConcreteSupplier(new Supply(warehouse,ProductType.APPLE)).getSupplyable();
        Supplyable supplier2 = new ConcreteSupplier(new Supply(warehouse,ProductType.BANANA)).getSupplyable();
        Supplyable supplier3 = new ConcreteSupplier(new Supply(warehouse,ProductType.ORANGE)).getSupplyable();
        
        warehouse.registerSupplier(supplier1);
        warehouse.registerSupplier(supplier2);
        warehouse.registerSupplier(supplier3);
        
        consumer1.buy();
        supplier2.sale();
        supplier3.sale();
        consumer3.buy();
        consumer1.buy();
        
        
    }
    
}
