package com.voitech.devtest.slowdictionary;

import java.util.Random;
import java.util.Set;
import org.testng.annotations.Test;

/**
 *
 * @author wojtek
 */
public class RefractoredDictionaryNGTest {

    private final Random random = new Random();
    private final RefactoredDictionary instance = new RefactoredDictionary();

    @Test(threadPoolSize = 1000, invocationCount = 500)
    public void testAddToDictionary() {

        for (int i = 0; i < 10000; i++) {
            instance.addToDictionary(String.valueOf(random.nextInt(10000)), "translation");
        }
        Set result = instance.getAllWords();

        System.out.println("Refracotred result.size()= " + result.size());
    }

    @Test(threadPoolSize = 1000, invocationCount = 500)
    public void testTranslate() {

        for (int i = 0; i < 10000; i++) {
            instance.translate(String.valueOf(random.nextInt(10000)));
        }
    }
}
